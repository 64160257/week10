package week10.example;

public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;
    public Triangle(double a, double b,double c){
        super("triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public double calArea() {
        double s = (a+b+c)/2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }
    @Override
    public double calPerimeter() {
        return a + b + c;
    }
    @Override
    public String toString() {
        return this.getName() + " triangle: "+this.a + " " + this.b + " " + this.c;
    }
}
